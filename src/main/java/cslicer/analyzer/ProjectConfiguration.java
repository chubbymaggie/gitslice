package cslicer.analyzer;

/*
 * #%L
 * CSlicer
 *    ______ _____  __ _                  
 *   / ____// ___/ / /(_)_____ ___   _____
 *  / /     \__ \ / // // ___// _ \ / ___/
 * / /___  ___/ // // // /__ /  __// /
 * \____/ /____//_//_/ \___/ \___//_/
 * %%
 * Copyright (C) 2014 - 2015 Department of Computer Science, University of Toronto
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import java.util.HashSet;
import java.util.Set;

import org.apache.commons.io.FilenameUtils;

import cslicer.builder.UnitTestScope;

public class ProjectConfiguration {

	private String fRepoPath = null;
	private String fStartId = null;
	private String fEndId = null; // optional - default HEAD
	private String fBuildPath = null;
	private String fSubModuleBuildPath = null; // optional
	private String fProjectPath = null; // optional
	private UnitTestScope fTestScope = new UnitTestScope(); // optional
	private BUILD_SYSTEM fBuildSystem = BUILD_SYSTEM.MAVEN; // optional
	private boolean fOutputEnabled = false; // optional
	private String fJDKVersion = DEFAULT_JDK; // optional. default: 1.7
	private int fHistoryLength = -1; // optional. default -1. the number of
										// commits to trace back from end
	// if jacoco exec is provided then no need for build and module path, or
	// tests
	private String fTestJacocoExecPath = null; // optional
	private String fSourceRootPath = null; // optional
	private String fClassRootPath = null; // optional
	private Set<String> fExcludedPath = new HashSet<String>();
	private String fTouchSetPath = null;

	public enum BUILD_SYSTEM {
		MAVEN, ANT
	}

	public static final String DEFAULT_JDK = "1.7";

	public boolean isConsistent() {
		if (fRepoPath == null || (fStartId == null && fHistoryLength < 0)
				|| fBuildPath == null)
			return false;
		if ((fTestJacocoExecPath == null) != (fSourceRootPath == null)
				|| (fTestJacocoExecPath == null) != (fClassRootPath == null)
				|| (fClassRootPath == null) != (fSourceRootPath == null))
			return false;
		return true;
	}

	public ProjectConfiguration setAnalysisLength(int len) {
		fHistoryLength = len;
		return this;
	}

	public int getAnalysisLength() {
		if (fHistoryLength > 0)
			return fHistoryLength;
		else
			return 0;
	}

	public ProjectConfiguration setTouchSetPath(String path) {
		fTouchSetPath = path;
		return this;
	}

	public String getTouchSetPath() {
		return fTouchSetPath;
	}

	public ProjectConfiguration setProjectJDKVersion(String version) {
		fJDKVersion = version;
		return this;
	}

	public String getProjectJDKVersion() {
		return fJDKVersion;
	}

	public ProjectConfiguration setJacocoExecPath(String execPath) {
		fTestJacocoExecPath = execPath;
		return this;
	}

	public String getJacocoExecPath() {
		return fTestJacocoExecPath;
	}

	public boolean isJacocoExecPathSet() {
		return fTestJacocoExecPath != null;
	}

	public ProjectConfiguration setSourceRootPath(String sourcePath) {
		fSourceRootPath = sourcePath;
		return this;
	}

	public String getSourceRootPath() {
		return fSourceRootPath;
	}

	public boolean isSourceRootPathSet() {
		return fSourceRootPath != null;
	}

	public ProjectConfiguration setClassRootPath(String classPath) {
		fClassRootPath = classPath;
		return this;
	}

	public String getClassRootPath() {
		return fClassRootPath;
	}

	public boolean isClassRootPathSet() {
		return fClassRootPath != null;
	}

	public ProjectConfiguration setSubModuleBuildScriptPath(String buildPath) {
		fSubModuleBuildPath = buildPath;
		return this;
	}

	public ProjectConfiguration setRepositoryPath(String repoPath) {
		fRepoPath = repoPath;
		return this;
	}

	public ProjectConfiguration setStartCommitId(String commitId) {
		fStartId = commitId;
		return this;
	}

	public ProjectConfiguration setEndCommitId(String commitId) {
		fEndId = commitId;
		return this;
	}

	public ProjectConfiguration setBuildScriptPath(String buildPath) {
		fBuildPath = buildPath;
		return this;
	}

	public ProjectConfiguration setProjectPath(String projectPath) {
		this.fProjectPath = projectPath;
		return this;
	}

	public ProjectConfiguration setBuildSystem(BUILD_SYSTEM buildSys) {
		this.fBuildSystem = buildSys;
		return this;
	}

	public ProjectConfiguration setTestCases(UnitTestScope scope) {
		this.fTestScope = scope;
		return this;
	}

	public ProjectConfiguration setEnableOutput(boolean enableOutput) {
		this.fOutputEnabled = enableOutput;
		return this;
	}

	public String getRepositoryPath() {
		return this.fRepoPath;
	}

	public String getStartCommitId() {
		return this.fStartId;
	}

	public String getEndCommitId() {
		return this.fEndId;
	}

	public String getBuildScriptPath() {
		return this.fBuildPath;
	}

	public String getSubModuleBuildScriptPath() {
		if (fSubModuleBuildPath == null)
			return fBuildPath;
		return this.fSubModuleBuildPath;
	}

	/**
	 * The path to the project base directory. Use the root directory of the
	 * repository path by default.
	 * 
	 * @return a {@code String} representation of the project path
	 */
	public String getProjectPath() {
		if (fProjectPath == null)
			return FilenameUtils.getFullPath(FilenameUtils
					.normalizeNoEndSeparator(fRepoPath));
		return this.fProjectPath;
	}

	public BUILD_SYSTEM getBuildSystem() {
		return this.fBuildSystem;
	}

	/**
	 * Get the names of interested test cases.
	 * 
	 * @return {@code null} if test suite is not set, then by default the whole
	 *         set is used
	 */
	public UnitTestScope getTestCases() {
		return this.fTestScope;
	}

	/**
	 * Get the project output setting.
	 * 
	 * @return {@code true} if output is enabled
	 */
	public boolean isOutputEnabled() {
		return this.fOutputEnabled;
	}

	public boolean isSubModuleSet() {
		return this.fSubModuleBuildPath != null;
	}

	public Set<String> getExcludedPaths() {
		return fExcludedPath;
	}

	public ProjectConfiguration setExcludedPaths(Set<String> excludes) {
		fExcludedPath = excludes;
		return this;
	}
}
