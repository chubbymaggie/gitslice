package cslicer.analyzer;

/*
 * #%L
 * CSlicer
 *    ______ _____  __ _                  
 *   / ____// ___/ / /(_)_____ ___   _____
 *  / /     \__ \ / // // ___// _ \ / ___/
 * / /___  ___/ // // // /__ /  __// /
 * \____/ /____//_//_/ \___/ \___//_/
 * %%
 * Copyright (C) 2014 - 2015 Department of Computer Science, University of Toronto
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Hashtable;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.Stack;

import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.tuple.Pair;
import org.apache.tools.ant.util.StringUtils;
import org.eclipse.jgit.lib.Ref;
import org.eclipse.jgit.revwalk.RevCommit;

import ch.uzh.ifi.seal.changedistiller.model.entities.Delete;
import ch.uzh.ifi.seal.changedistiller.model.entities.Insert;
import ch.uzh.ifi.seal.changedistiller.model.entities.Move;
import ch.uzh.ifi.seal.changedistiller.model.entities.SourceCodeChange;
import ch.uzh.ifi.seal.changedistiller.model.entities.SourceCodeEntity;
import ch.uzh.ifi.seal.changedistiller.model.entities.Update;
import cslicer.analyzer.SlicingResult.REFACTOR_FLAG;
import cslicer.builder.BuildScriptInvalidException;
import cslicer.builder.UnitTestScope;
import cslicer.builder.maven.MavenInvoker;
import cslicer.callgraph.BcelStaticCallGraphBuilder;
import cslicer.callgraph.CGNode;
import cslicer.callgraph.ClassPathInvalidException;
import cslicer.callgraph.StaticCallGraphBuilder;
import cslicer.coverage.CoverageAnalyzer;
import cslicer.coverage.CoverageControlIOException;
import cslicer.coverage.CoverageDataMissingException;
import cslicer.coverage.CoverageDatabase;
import cslicer.coverage.CoverageDatabase.CoverageStore;
import cslicer.distiller.ChangeDistillerException;
import cslicer.distiller.ChangeExtractor;
import cslicer.distiller.GitRefSourceCodeChange;
import cslicer.jgit.AmbiguousEndPointException;
import cslicer.jgit.BranchNotFoundException;
import cslicer.jgit.CheckoutFileFailedException;
import cslicer.jgit.CommitNotFoundException;
import cslicer.jgit.CreateBranchFailedException;
import cslicer.jgit.DeleteBranchFailedException;
import cslicer.jgit.JGit;
import cslicer.jgit.RepositoryInvalidException;
import cslicer.utils.BytecodeUtils;
import cslicer.utils.DependencyCache;
import cslicer.utils.PrintUtils;
import cslicer.utils.PrintUtils.TAG;
import cslicer.utils.StatsUtils;

/**
 * Implementations of the semantic slicing algorithms.
 * 
 * @author Yi Li
 * @since JDK1.7
 */
public class Slicer {
	public enum PRUNE_STRATEGY {
		NEW_TO_OLD, OLD_TO_NEW, SIGNIFICANCE, RANDOM
	}

	private static int fSnapCounter; // index for snap shot branch
	private JGit fJGit;
	private final String fRepoPath;
	private final RevCommit fStart;
	private final RevCommit fEnd; // optional: could be null

	private List<RevCommit> fHistory;
	private final UnitTestScope fTests; // name of the target test cases

	private Stack<String> fSnapShots;
	private Hashtable<String, LinkedHashMap<RevCommit, RevCommit>> fCommitMap;

	private final MavenInvoker fCompiler;
	private final CoverageAnalyzer fCoverage; // test coverage analyzer

	private StaticCallGraphBuilder fCallGraph; // call graph builder
	private SlicingResult fResult;

	private Hashtable<String, Integer> fSignificance; // significance level

	private Set<String> fExcludedPaths;
	// test touching set
	private TouchSet fTestTouchSet;

	private String fTouchSetPath;

	private final ProjectConfiguration fConfig;

	private LinkedList<RevCommit> A;
	private LinkedList<RevCommit> D;

	private Set<String> fIgnoreFilesTotal; // files in which changes ignored

	private VersionTracker fTracker; // track version of each changed entity

	public Slicer(ProjectConfiguration config)
			throws RepositoryInvalidException, CommitNotFoundException,
			BuildScriptInvalidException, CoverageControlIOException,
			AmbiguousEndPointException, ProjectConfigInvalidException,
			BranchNotFoundException, CoverageDataMissingException {

		// load configurations
		if (!config.isConsistent())
			throw new ProjectConfigInvalidException();

		fConfig = config;

		fRepoPath = config.getRepositoryPath();
		fJGit = new JGit(this.fRepoPath);
		fEnd = fJGit.getCommit(config.getEndCommitId());

		// if optional history length is provided
		if (config.getAnalysisLength() > 0) {
			fHistory = fJGit.getCommitList(fEnd, config.getAnalysisLength(),
					true);
			fStart = fHistory.get(0);
		} else {
			fStart = fJGit.getCommit(config.getStartCommitId());
			// history is a list of commits from target.child -> head
			fHistory = fJGit.getCommitList(fStart, fEnd, true);
		}

		fTests = config.getTestCases();
		fExcludedPaths = config.getExcludedPaths();

		fIgnoreFilesTotal = new HashSet<String>();
		fTracker = new VersionTracker();

		// initialization
		fSnapShots = new Stack<String>();
		fSignificance = new Hashtable<String, Integer>();
		fTestTouchSet = new TouchSet();
		fTouchSetPath = config.getTouchSetPath();

		fSnapShots.push(fJGit.getCurrentBranchName());
		fSnapCounter = 0;

		PrintUtils.print("Original |H|: " + fHistory.size());

		// initialize commit map
		fCommitMap = new Hashtable<String, LinkedHashMap<RevCommit, RevCommit>>();
		LinkedHashMap<RevCommit, RevCommit> mapping = new LinkedHashMap<RevCommit, RevCommit>();
		for (RevCommit h : fHistory) {
			mapping.put(h, h);
			fSignificance.put(h.getName(), 0);
		}
		fCommitMap.put(fSnapShots.peek(), mapping);

		// setup compilation checker
		fCompiler = new MavenInvoker(config.getBuildScriptPath(),
				config.isOutputEnabled());

		// setup coverage analyzer
		if (config.isJacocoExecPathSet() && config.isClassRootPathSet()
				&& config.isSourceRootPathSet())
			fCoverage = new CoverageAnalyzer(config.getJacocoExecPath(),
					config.getSourceRootPath(), config.getClassRootPath());
		else
			fCoverage = (config.isSubModuleSet())
					? new CoverageAnalyzer(fCompiler,
							config.getSubModuleBuildScriptPath())
					: new CoverageAnalyzer(fCompiler);

		// initialize lists to be used by the slicing algorithm
		D = new LinkedList<RevCommit>();
		A = new LinkedList<RevCommit>(fHistory);
		fResult = new SlicingResult(A);
	}

	@SuppressWarnings("unused")
	private boolean checkCompile() {
		return fCompiler.checkCompilation();
	}

	/**
	 * Restore repository state. Cleanup generated temporary files. Can only be
	 * called once.
	 */
	public void cleanUp() {
		try {
			fCompiler.restoreBuildFile();
		} catch (IOException e) {
			PrintUtils.print("Build file not restored!", TAG.WARNING);
		}
		fCompiler.cleanUp();
	}

	private String commitSummary(RevCommit c) {
		return c.abbreviate(8).name() + " : " + c.getShortMessage();
	}

	private List<RevCommit> computeHunkDepSet(
			final Collection<RevCommit> cSet) {
		DependencyCache cache = new DependencyCache();

		int i = 0;
		for (RevCommit a : cSet) {
			if (cache.directDepsComputed(a)) {
				continue;
			} else {
				fJGit.findHunkDependencies(a, new HashSet<RevCommit>(A), cache,
						fExcludedPaths);
			}

			i++;
			PrintUtils.printProgress("", i * 100 / cSet.size());
		}

		PrintUtils.print(cache);

		// return cache.getSortedDeps(true);

		List<RevCommit> res = new LinkedList<RevCommit>();
		Set<RevCommit> deps = cache.getUnSortedDeps();
		for (RevCommit h : fHistory) {
			if (deps.contains(h))
				res.add(h);
		}

		return res;
	}

	public List<RevCommit> computeHunkDepSetWithId(
			final Collection<String> cSet) {
		Set<RevCommit> set = new HashSet<RevCommit>();

		for (String id : cSet) {
			try {
				set.add(fJGit.getCommit(id));
			} catch (CommitNotFoundException e) {
				PrintUtils.print("Provided commit not found in the repo!",
						TAG.WARNING);
				return Collections.emptyList();
			}
		}
		return computeHunkDepSet(set);
	}

	private void computeTestTouchSet(boolean skip) {
		if (fTouchSetPath != null && fTestTouchSet.loadFromFile(fTouchSetPath))
			return;

		try {
			// do coverage analysis on the latest version
			PrintUtils.print("Running coverage analysis ...");
			StatsUtils.resume("tests.time");
			CoverageDatabase cd;
			if (fTests == null) {
				// test scope provided
				cd = fCoverage.analyseCoverage();
			} else {
				// run specified tests
				cd = fCoverage.analyseCoverage(fTests);
			}
			CoverageStore store = cd.getCoverageStore();
			StatsUtils.stop("tests.time");

			PrintUtils.print("Drawing static call graph ...");
			StatsUtils.resume("call.graph.time");
			// build static call graph and include compilation dependencies
			fCallGraph = skip ? new BcelStaticCallGraphBuilder()
					: new BcelStaticCallGraphBuilder(fCoverage.getClassPath(),
							store.getPartiallyCoveredClassNames());

			fCallGraph.buildCallGraph();
			fCallGraph.getCallGraph().printCallGraph();

			StatsUtils.stop("call.graph.time");

			PrintUtils.print("Adding to touch set ...");
			for (SourceCodeEntity c : store.getAllRelevantEntities()) {
				fTestTouchSet.addToTestSet(c.getUniqueName(), c);
				StatsUtils.count("test.deps");
				// compute compilation dependencies using static extended call
				// graph. filter generic type in unique names
				for (CGNode compDep : fCallGraph.getCallGraph()
						.getTransitiveSuccessors(BytecodeUtils
								.filterGenericType(c.getUniqueName()))) {
					fTestTouchSet.addToCompSet(compDep.getName());
				}
			}

		} catch (CoverageControlIOException | ClassPathInvalidException e) {
			PrintUtils.print("Test touch computation failed!",
					PrintUtils.TAG.WARNING);
			e.printStackTrace();
		} finally {
			// clean up
			// fCompiler.cleanUp();
		}

		PrintUtils.print(fTestTouchSet.toString());
		if (fTouchSetPath != null)
			fTestTouchSet.saveToFile(fTouchSetPath);
	}

	@SuppressWarnings("unused")
	private void computeTestTouchSet(String entryPoint) {
		try {
			// do coverage analysis on the latest version
			StatsUtils.resume("tests.time");
			CoverageDatabase cd;
			if (fTests == null) {
				// test scope provided
				cd = fCoverage.analyseCoverage();
			} else {
				// run specified tests
				cd = fCoverage.analyseCoverage(fTests);
			}
			CoverageStore store = cd.getCoverageStore();
			StatsUtils.stop("tests.time");

			StatsUtils.resume("call.graph.time");
			// build static call graph and include compilation dependencies
			fCallGraph = new BcelStaticCallGraphBuilder(
					fCoverage.getClassPath(), null);
			fCallGraph.buildCallGraph();
			// fCallGraph.getCallGraph().printCallGraph();
			StatsUtils.stop("call.graph.time");

			Set<String> nodes = fCallGraph.getCallGraph()
					.getTransitiveSuccessorNames(entryPoint);
			nodes.add(entryPoint);

			for (SourceCodeEntity c : store.getAllRelevantEntities()) {
				if (!nodes.contains(c.getUniqueName()))
					continue;

				fTestTouchSet.addToTestSet(c.getUniqueName(), c);

				StatsUtils.count("test.deps");
				// compute compilation dependencies using static extended call
				// graph. filter generic type in unique names
				for (CGNode compDep : fCallGraph.getCallGraph()
						.getTransitiveSuccessors(BytecodeUtils
								.filterGenericType(c.getUniqueName()))) {
					fTestTouchSet.addToCompSet(compDep.getName());
					StatsUtils.count("comp.deps");
				}
			}

		} catch (CoverageControlIOException | ClassPathInvalidException e) {
			PrintUtils.print("Test touch computation failed!",
					PrintUtils.TAG.WARNING);
			e.printStackTrace();
		} finally {
			// clean up
			fCompiler.cleanUp();
		}

		PrintUtils.print(fTestTouchSet.toString());
	}

	@SuppressWarnings("unused")
	private LinkedHashMap<RevCommit, RevCommit> currentMapping() {
		assert(fCommitMap.keySet().contains(fSnapShots.peek()));
		assert(fCommitMap.get(fSnapShots.peek()) != null);
		return fCommitMap.get(fSnapShots.peek());
	}

	@SuppressWarnings("unused")
	private String currentSnapName() {
		return fSnapShots.peek();
	}

	public List<RevCommit> doSlicing() throws CommitNotFoundException {
		return doSlicing(false, false);
	}

	/**
	 * Implementation of the CSLICER semantic slicing algorithm.
	 * 
	 * @param skipHunk
	 *            skip hunk dependency computation
	 * @param skipCallGraph
	 *            skip call graph construction
	 * @return a list of {@link RevCommit} to drop
	 * @throws CommitNotFoundException
	 */
	public List<RevCommit> doSlicing(boolean skipHunk, boolean skipCallGraph)
			throws CommitNotFoundException {

		// initialize the set of files need to be reverted in the end
		Set<String> keepFilesTotal = new HashSet<String>();

		// compute touched source code entities by the test
		computeTestTouchSet(skipCallGraph);

		StatsUtils.resume("main.algo");

		Collections.reverse(A);

		PrintUtils.print("Initial |S| = " + A.size(), PrintUtils.TAG.OUTPUT);

		ChangeExtractor extractor = new ChangeExtractor(fJGit,
				fConfig.getProjectJDKVersion());

		// inspecting commits from newest to oldest
		int i = 0;
		PrintUtils.print("Analysing Commits: ", TAG.OUTPUT);
		for (RevCommit c : A) {

			PrintUtils.print(
					"=== Inspecting commit: " + commitSummary(c) + " ===");

			Set<GitRefSourceCodeChange> changes;
			try {
				changes = extractor.extractChanges(c);
			} catch (ChangeDistillerException e) {
				e.printStackTrace();
				continue;
			}

			Set<SourceCodeEntity> touchGrowth = new HashSet<SourceCodeEntity>();
			Set<SourceCodeEntity> compGrowth = new HashSet<SourceCodeEntity>();
			boolean hitTest = false;
			boolean hitComp = false;

			// initialize the set of files need to be reverted per commit
			Set<String> keepFiles = new HashSet<String>();
			Set<String> ignoreFiles = new HashSet<String>();

			// compute the set of all changed entities in this commit
			// for (SourceCodeChange change : changes)
			// touchGrowth.add(change.getChangedEntity());

			for (GitRefSourceCodeChange gitChange : changes) {

				// get change distiller change
				SourceCodeChange change = gitChange.getSourceCodeChange();
				// get file path to changed entity
				String filePath = gitChange.getChangedFilePath();

				// testTouchSet is the set of field/method that are touched
				// by the tests
				// parent entity is field/method/class which contains the
				// change
				if (change instanceof Delete) {
					// do nothing for delete, since it shouldn't appear
					// in the touch set
					// XXX need to consider lookup changes here
					ignoreFiles.add(filePath);
					continue;
				} else if (change instanceof Insert) {
					Insert ins = (Insert) change;
					String uniqueName = ins.getChangedEntity().getUniqueName();
					hitTest |= fTestTouchSet.hitTestSet(uniqueName);
					hitComp |= fTestTouchSet.hitCompSet(uniqueName);

					// files are scheduled for revert which contain atomic
					// changes that are not kept
					if (fTestTouchSet.hitTestSet(uniqueName)
							|| fTestTouchSet.hitCompSet(uniqueName)) {
						keepFiles.add(filePath);

						fTracker.trackEntity(uniqueName, filePath, c.getName());
					} else {
						ignoreFiles.add(filePath);
					}

				} else if (change instanceof Update) {
					Update upd = (Update) change;
					String uniqueName = upd.getNewEntity().getUniqueName();

					// grow FUNC set
					if (fTestTouchSet.hitTestSet(uniqueName)) {
						hitTest |= true;
						touchGrowth.add(change.getChangedEntity());

						keepFiles.add(filePath);
					}
					// XXX verify that change of signature is treated as add
					// and remove!

					// grow COMP set
					boolean signatureChange = !upd.getChangedEntity()
							.getUniqueName().equals(uniqueName);

					if (fTestTouchSet.hitCompSet(uniqueName)
							&& signatureChange) {
						hitComp |= true;
						compGrowth.add(change.getChangedEntity());

						keepFiles.add(filePath);
						
						fTracker.trackEntity(uniqueName, filePath, c.getName());
					}

					// ignore change
					if (!fTestTouchSet.hitTestSet(uniqueName)
							&& !(fTestTouchSet.hitCompSet(uniqueName)
									&& signatureChange))
						ignoreFiles.add(filePath);

				} else if (change instanceof Move) {
					// shouldn't detect move for structure nodes
					assert false;
				} else
					assert false;
			}

			// grow touch set if the commit is affecting
			if (hitTest | hitComp) {
				for (SourceCodeEntity g : touchGrowth)
					fTestTouchSet.addToTestSet(g.getUniqueName(), g);
				for (SourceCodeEntity g : compGrowth) {
					fTestTouchSet.addToTestSet(g.getUniqueName(), g);
					
				}

				if (hitTest) {
					fResult.add(c, REFACTOR_FLAG.TEST);
				} else if (hitComp) {
					fResult.add(c, REFACTOR_FLAG.COMP);
				}

				keepFilesTotal.addAll(keepFiles);
				ignoreFiles.removeAll(keepFiles);
				fIgnoreFilesTotal.addAll(ignoreFiles);

			} else {
				D.add(c); // drop set from newest to oldest
			}

			i++;
			PrintUtils.print("");
			PrintUtils.printProgress("", i * 100 / A.size());
		}

		StatsUtils.stop("main.algo");

		// ---------------------------------------------------------------
		// compute hunk dependencies
		// the target commit is the latest commit in history
		// ---------------------------------------------------------------
		PrintUtils.breakLine();
		PrintUtils.print("Analysing Hunk Dependency: ", TAG.OUTPUT);
		Set<RevCommit> keep = new HashSet<RevCommit>(A);
		keep.removeAll(D);

		StatsUtils.resume("hunk.deps.time");
		List<RevCommit> hunkDeps = new ArrayList<RevCommit>();
		if (skipHunk)
			hunkDeps.addAll(keep);
		else
			hunkDeps = computeHunkDepSet(keep);

		StatsUtils.stop("hunk.deps.time");
		for (RevCommit d : Collections.unmodifiableList(D)) {
			if (hunkDeps.contains(d)) {
				PrintUtils.print("Hunk depends on: " + commitSummary(d)
						+ " : added back.");
				keep.add(d);
				StatsUtils.count("hunk.deps.set");
			}
		}

		fResult.markDropAsHunk(hunkDeps);
		D.removeAll(keep);

		// ---------------------------------------------------------------
		// output semantic slicing summary
		// ---------------------------------------------------------------
		PrintUtils.breakLine();
		PrintUtils.print("Original |H| " + A.size());
		PrintUtils.print(
				"Reduced |H| = " + (keep.size()) + ", Reduction Rate: "
						+ ((float) D.size() * 100 / (float) A.size()) + "%",
				PrintUtils.TAG.OUTPUT);
		PrintUtils.print("Semantic Deps Set: "
				+ (keep.size() - StatsUtils.readCount("hunk.deps.set")));
		PrintUtils.print(fResult.toString(), TAG.OUTPUT);

		// final files to be reverted
		fIgnoreFilesTotal.removeAll(keepFilesTotal);

		// clean up
		// fJGit.cleanRepo();

		return Collections.unmodifiableList(hunkDeps);
	}

	/**
	 * Dummy {@code doSlicing} method where H' is given.
	 * 
	 * @param keep
	 *            sliced sub-history H'
	 * @return H' plus hunk dependencies
	 */
	public List<RevCommit> doSlicing(Set<String> keep) {
		List<RevCommit> hunkDeps = computeHunkDepSetWithId(keep);

		Set<String> hunkId = new HashSet<String>();
		for (RevCommit h : hunkDeps)
			hunkId.add(h.getName());

		List<RevCommit> drop = new LinkedList<RevCommit>();

		Collections.reverse(A);
		for (RevCommit a : A) {
			if (hunkId.contains(a.getName())) {
				PrintUtils.print("hunk: " + commitSummary(a));
				continue;
			}
			if (keep.contains(a.getName())) {
				PrintUtils.print("keep: " + commitSummary(a));
				continue;
			}

			drop.add(a);
			PrintUtils.print("Drop: " + commitSummary(a));
		}

		return drop;
	}

	private String freshSnapName() {
		return "SNAPSHOT" + (fSnapCounter++);
	}

	protected Set<String> getReducedSet() {
		Set<String> dIds = new HashSet<String>();
		for (RevCommit d : D)
			dIds.add(commitSummary(d));
		return dIds;
	}

	public final SlicingResult getSlicingReseult() {
		return fResult;
	}

	/**
	 * Return a view of the partial history starting from {@code commit}
	 * inclusive.
	 * 
	 * @param commit
	 *            the starting {@code RevCommit}
	 * @return list of {@code RevCommit} in the remaining history
	 */
	private final List<RevCommit> getRemainingHistory(RevCommit commit) {
		int index = fHistory.indexOf(commit);
		if (index == -1)
			return Collections.emptyList();

		return Collections
				.unmodifiableList(fHistory.subList(index, fHistory.size()));
	}

	/**
	 * @param commit
	 *            target {@link RevCommit}
	 * @return the significance level computed so far for target commit
	 */
	public int getSignificance(RevCommit commit) {
		if (fSignificance.contains(commit.getName())) {
			return fSignificance.get(commit.getName());
		}
		return 0;
	}

	/**
	 * Map {@link RevCommit} to the duplicate on the current snapshot branch.
	 * 
	 * @param origCommit
	 *            {@code RevCommit} on the original branch
	 * @return corresponding commit on the latest snapshot branch
	 */
	private RevCommit resolveSnapCommit(RevCommit origCommit) {
		RevCommit res = origCommit;
		for (String snap : fSnapShots) {
			if (fCommitMap.get(snap).containsKey(res)
					&& fCommitMap.get(snap).get(res) != null)
				res = fCommitMap.get(snap).get(res);
		}
		return res;
	}

	private boolean tryPickCommits(List<RevCommit> toPick) {
		if (toPick.isEmpty())
			return true;

		try {
			PrintUtils.print("Begin picking commits ...");
			// create a new snapshot branch from parent of root
			Ref snap = fJGit.createNewBranch(freshSnapName(), fStart);
			fSnapShots.push(snap.getName());

			LinkedHashMap<RevCommit, RevCommit> mapping = new LinkedHashMap<RevCommit, RevCommit>();
			for (RevCommit p : toPick) {
				mapping.put(p, null);
			}

			if (!fJGit.pickCommitsToBranch(snap.getName(), mapping, false)) {
				undoRemoveCommit(false);
				return false;
			}

			fCommitMap.put(snap.getName(), mapping);

			PrintUtils.print("Finish picking commits ...");
			return true;
		} catch (CreateBranchFailedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return false;
	}

	@SuppressWarnings("unused")
	private boolean tryRemoveCommit(List<RevCommit> toRemove) {
		if (toRemove.isEmpty())
			return true;

		// XXX assume the toRemove set is sorted from newest to oldest
		RevCommit root = resolveSnapCommit(toRemove.get(toRemove.size() - 1));
		// XXX assume only one parent
		RevCommit parent = fJGit.getParentCommits(root).get(0);

		try {
			PrintUtils.print("Begin removing commits ...");
			PrintUtils.print("Root for removal: " + root.getShortMessage());

			// create a new snapshot branch from parent of root
			Ref snap = fJGit.createNewBranch(freshSnapName(), parent);
			fSnapShots.push(snap.getName());

			LinkedHashMap<RevCommit, RevCommit> mapping = new LinkedHashMap<RevCommit, RevCommit>();
			for (RevCommit p : getRemainingHistory(root)) {
				mapping.put(p, null);
			}

			if (!fJGit.pickCommitsToBranch(snap.getName(), mapping,
					new HashSet<RevCommit>(toRemove))) {
				undoRemoveCommit(false);
				return false;
			}

			fCommitMap.put(snap.getName(), mapping);

			PrintUtils.print("Finishing remove commits ...");
			return true;
		} catch (CreateBranchFailedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return false;
	}

	/**
	 * Revert selected files silently.
	 * 
	 * @return {@code true} if revert successes
	 */
	protected boolean tryRevertFiles() {
		try {
			fJGit.checkOutFiles(fIgnoreFilesTotal, fStart);
			return true;
		} catch (CheckoutFileFailedException e) {
			e.printStackTrace();
		}
		return false;
	}

	/**
	 * Undo commit removal and restore to the original(HEAD) state.
	 */
	protected void undoRemoveCommit(boolean hard) {
		String orighead = fSnapShots.pop();
		assert(fJGit.getCurrentBranchName().equals(orighead));

		if (hard)
			fJGit.resetHeadHard();

		fJGit.checkOutExistingBranch(fSnapShots.peek());
		try {
			fJGit.deleteBranch(orighead);
			PrintUtils.print("Undo " + orighead + " and go back to "
					+ fSnapShots.peek());
		} catch (DeleteBranchFailedException e) {
			PrintUtils.print("Delete of branch " + orighead + " has failed!");
			e.printStackTrace();
		}
	}

	/**
	 * Cherry-picking the computed subset. Verify that there is not conflict.
	 * 
	 * @param pick
	 *            set of {@link RevCommit} to pick
	 * @return {@code true} if slicing does not cause conflict.
	 */
	public boolean verifyResultPicking(List<RevCommit> pick) {
		// try remove commits
		if (!tryPickCommits(pick))
			return false;

		return true;
	}

	/**
	 * Verify that the sliced history passes the tests.
	 * 
	 * @param pick
	 *            set of {@link RevCommit} to pick
	 * @return {@code true} if the slice does not cause conflict and the tests
	 *         pass
	 */
	public boolean verifyResultTestPassing(List<RevCommit> pick) {
		// try remove commits
		if (!tryPickCommits(pick))
			return false;

		// revert files to be ignored
		if (!tryRevertFiles())
			return false;

		// run test
		// XXX assume junit is in the dependencies
		boolean pass = fCompiler.runSingleTest(fTests);
		fCompiler.cleanUp();

		return pass;
	}

	/**
	 * Verify the slicing results with given ID of dropped commits.
	 * 
	 * @param dropId
	 *            a list of commit ID
	 * @return {@code true} if test passes
	 */
	public boolean verifyResultWithId(List<String> dropId) {
		List<RevCommit> drop = new LinkedList<RevCommit>();

		for (String id : dropId) {
			try {
				drop.add(fJGit.getCommit(id));
			} catch (CommitNotFoundException e) {
				PrintUtils.print("Provided commit not found in the repo!",
						TAG.WARNING);
				return false;
			}
		}
		return verifyResultTestPassing(drop);
	}
}

/**
 * Slicing result. Assign each commit with a flag.
 * 
 * @author Yi Li
 *
 */
class SlicingResult {

	public enum REFACTOR_FLAG {
		DROP, TEST, COMP, HUNK
	}

	private LinkedHashMap<RevCommit, REFACTOR_FLAG> fCommits;

	public SlicingResult(List<RevCommit> history) {
		fCommits = new LinkedHashMap<RevCommit, REFACTOR_FLAG>();
		for (RevCommit h : history)
			fCommits.put(h, REFACTOR_FLAG.DROP);
	}

	/**
	 * Add flag for a commit.
	 * 
	 * @param c
	 * @param f
	 */
	public void add(RevCommit c, REFACTOR_FLAG f) {
		fCommits.put(c, f);
	}

	/**
	 * Get label for a commit.
	 * 
	 * @param c
	 * @return
	 */
	public REFACTOR_FLAG getLabel(RevCommit c) {
		return fCommits.get(c);
	}

	/**
	 * Get the whole labeled history.
	 * 
	 * @return
	 */
	public List<Pair<RevCommit, REFACTOR_FLAG>> getLabeledHistory() {
		List<Pair<RevCommit, REFACTOR_FLAG>> res = new LinkedList<Pair<RevCommit, REFACTOR_FLAG>>();
		for (RevCommit c : fCommits.keySet()) {
			res.add(Pair.of(c, fCommits.get(c)));
		}
		return res;
	}

	/**
	 * Label all remaining commits as HUNK.
	 * 
	 * @param keep
	 */
	public void markDropAsHunk(Collection<RevCommit> keep) {
		for (RevCommit k : keep) {
			if (fCommits.get(k) == REFACTOR_FLAG.DROP)
				fCommits.put(k, REFACTOR_FLAG.HUNK);
		}
	}

	@Override
	public String toString() {
		StringBuilder res = new StringBuilder();

		int hunkDeps = 0, semDeps = 0;

		for (RevCommit a : fCommits.keySet()) {

			if (getLabel(a).equals(REFACTOR_FLAG.TEST)
					|| getLabel(a).equals(REFACTOR_FLAG.COMP))
				semDeps++;
			if (!getLabel(a).equals(REFACTOR_FLAG.DROP))
				hunkDeps++;

			res.append(fCommits.get(a) + ": " + a.abbreviate(8).name() + " : "
					+ a.getShortMessage());
			res.append("\n");
		}

		float size = (float) fCommits.size();

		res.append("Reduction Rates: " + (size - semDeps) * 100 / size + "%\n");
		res.append(
				"Reduction Hunks: " + (size - hunkDeps) * 100 / size + "%\n");

		return res.toString();
	}
}

/**
 * A set containing AST entities touched by tests. It also supports various
 * filters which ignore irrelevant entities.
 * 
 * @author liyi
 *
 */
class TouchSet {

	// set of entities directly executed by tests
	private Hashtable<String, SourceCodeEntity> testSet;
	private HashSet<String> testNameSet;
	// compilation dependencies of the test set
	private HashSet<String> compSet;

	private HashSet<String> testExclude;

	protected TouchSet() {
		testSet = new Hashtable<String, SourceCodeEntity>();
		testNameSet = new HashSet<String>();
		compSet = new HashSet<String>();
		testExclude = new HashSet<String>();

		// hardcode excludes
		testExclude.add("gitref.utils.PrintUtils");
	}

	/**
	 * Add a new element to the COMP set.
	 * 
	 * @param key
	 */
	public void addToCompSet(String key) {
		if (compSet.add(key))
			StatsUtils.count("comp.deps");
	}

	/**
	 * Add a new element to the TEST set.
	 * 
	 * @param key
	 * @param entity
	 */
	public void addToTestSet(String key, SourceCodeEntity entity) {
		String filterKey = BytecodeUtils.filterGenericType(key);
		// check excludes
		// for (String e : testExclude) {
		// if (key.contains(e))
		// return;
		// }

		testSet.put(filterKey, entity);
		testNameSet.add(filterKey);
	}

	/**
	 * Retrieve a TEST set element using key.
	 * 
	 * @param key
	 * @return
	 */
	public SourceCodeEntity getTestSetEntity(String key) {
		return testSet.get(BytecodeUtils.filterGenericType(key));
	}

	/**
	 * Return names of all entities touched.
	 * 
	 * @return
	 */
	public Set<String> getTouchSet() {
		HashSet<String> res = new HashSet<String>();
		res.addAll(testNameSet);
		res.addAll(compSet);
		return Collections.unmodifiableSet(res);
	}

	/**
	 * Return file paths of all touched entities.
	 * 
	 * @return
	 */
	public Set<String> getTouchSetFilePaths() {
		HashSet<String> res = new HashSet<String>();

		return Collections.unmodifiableSet(res);
	}

	/**
	 * Check whether a key hits the COMP set.
	 * 
	 * @param key
	 * @return
	 */
	public boolean hitCompSet(String key) {
		return compSet.contains(key);
	}

	/**
	 * Is the name key hit test set.
	 * 
	 * @param key
	 * @return
	 */
	public boolean hitTestSet(String key) {
		return testNameSet.contains(key);
	}

	/**
	 * Load TEST and COMP set data from file.
	 * 
	 * @param path
	 *            file path
	 * @return {@code true} if successful
	 */
	public boolean loadFromFile(String path) {
		try {
			String content = FileUtils
					.readFileToString(FileUtils.getFile(path));

			if (content.isEmpty())
				return false;

			for (String line : content.split(StringUtils.LINE_SEP)) {
				if (line.startsWith("Test:"))
					testNameSet.add(line.substring(6, line.length()).trim());
				else if (line.startsWith("Comp:"))
					compSet.add(line.substring(6, line.length()).trim());
			}
		} catch (IOException e) {
			PrintUtils.print("Read touch sets failed!");
			return false;
		}

		return true;
	}

	/**
	 * Save TEST and COMP set data to file.
	 * 
	 * @param path
	 *            file path
	 */
	public void saveToFile(String path) {
		try {
			FileUtils.writeStringToFile(FileUtils.getFile(path), toString());
			PrintUtils.print("Touch sets saved at " + path);
		} catch (IOException e) {
			PrintUtils.print("Save touch sets failed!");
			e.printStackTrace();
		}
	}

	@Override
	public String toString() {
		StringBuilder res = new StringBuilder();
		for (String k : testSet.keySet()) {
			res.append("Test: " + k);
			res.append("\n");
		}
		for (String c : compSet) {
			res.append("Comp: " + c);
			res.append("\n");
		}

		return res.toString();
	}
}

/**
 * Track the earliest possible version that a code entity should be updated to.
 * 
 * @author Yi Li
 *
 */
class VersionTracker {
	// entity -> a list of commits tracked
	private Map<String, List<String>> entityToVersionMap;
	private Map<String, Set<String>> fileToVersionMap;

	protected VersionTracker() {
		entityToVersionMap = new HashMap<String, List<String>>();
		fileToVersionMap = new HashMap<String, Set<String>>();
	}

	public void trackEntity(String entity, String file, String version) {
		if (!entityToVersionMap.containsKey(entity))
			entityToVersionMap.put(entity, new LinkedList<String>());

		if (!fileToVersionMap.containsKey(file))
			fileToVersionMap.put(file, new HashSet<String>());

		entityToVersionMap.get(entity).add(version);
		fileToVersionMap.get(file).add(version);
	}
}
