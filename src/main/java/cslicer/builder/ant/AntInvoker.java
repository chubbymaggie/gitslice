package cslicer.builder.ant;

/*
 * #%L
 * CSlicer
 *    ______ _____  __ _                  
 *   / ____// ___/ / /(_)_____ ___   _____
 *  / /     \__ \ / // // ___// _ \ / ___/
 * / /___  ___/ // // // /__ /  __// /
 * \____/ /____//_//_/ \___/ \___//_/
 * %%
 * Copyright (C) 2014 - 2015 Department of Computer Science, University of Toronto
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import java.io.File;
import java.io.IOException;

import org.apache.tools.ant.BuildException;
import org.apache.tools.ant.DefaultLogger;
import org.apache.tools.ant.Project;
import org.apache.tools.ant.ProjectHelper;
import org.apache.tools.ant.Target;
import org.apache.tools.ant.taskdefs.optional.junit.JUnitTask;
import org.apache.tools.ant.taskdefs.optional.junit.JUnitTest;
import org.jacoco.ant.CoverageTask;

import cslicer.builder.BuildScriptInvalidException;
import cslicer.builder.BuildToolInvoker;
import cslicer.builder.UnitTestScope;

public class AntInvoker extends BuildToolInvoker {

	private Project project;

	public AntInvoker(String script, boolean enableOutput)
			throws BuildScriptInvalidException {
		super(script);

		// Prepare Ant project
		project = new Project();
		File buildFile = scriptPath.toFile();
		project.setUserProperty("ant.file", buildFile.getAbsolutePath());

		DefaultLogger consoleLogger = new DefaultLogger();
		if (enableOutput) {
			consoleLogger.setErrorPrintStream(System.err);
			consoleLogger.setOutputPrintStream(System.out);
			consoleLogger.setMessageOutputLevel(Project.MSG_INFO);
			project.addBuildListener(consoleLogger);
		}

		project.init();
		ProjectHelper helper = ProjectHelper.getProjectHelper();
		project.addReference("ant.projectHelper", helper);
		helper.parse(project, scriptPath.toFile());

	}

	@Override
	public boolean checkCompilation() {
		try {
			project.fireBuildStarted();
			project.executeTarget("compile");
			project.fireBuildFinished(null);
		} catch (BuildException e) {
			e.printStackTrace();
			project.fireBuildFinished(null);
			return false;
		}

		return true;
	}

	@Override
	public boolean runUnitTests() {
		try {
			project.fireBuildStarted();
			project.executeTarget("test");
			project.fireBuildFinished(null);
		} catch (BuildException e) {
			e.printStackTrace();
			project.fireBuildFinished(null);
			return false;
		}

		return true;
	}

	@Override
	public void cleanUp() {
		try {
			project.fireBuildStarted();
			project.executeTarget("clean");
			project.fireBuildFinished(null);
		} catch (BuildException e) {
			e.printStackTrace();
			project.fireBuildFinished(null);
		}
	}

	private String addTestTarget(File build) {
		// a temporary file to store coverage information
		File jacoco_exec_file = org.codehaus.plexus.util.FileUtils
				.createTempFile("jacoco_", ".exec", build);
		jacoco_exec_file.deleteOnExit();

		project.addTaskDefinition("coverage", org.jacoco.ant.CoverageTask.class);

		Target target = new Target();
		target.setName("test");
		target.addDependency("compile");

		CoverageTask cover = (CoverageTask) project.createTask("coverage");
		cover.setTaskName("coverage");
		cover.setDestfile(jacoco_exec_file);
		cover.setAppend(false);

		JUnitTask junit = (JUnitTask) project.createTask("junit");
		junit.setTaskName("junit");
		junit.setFork(true);

		JUnitTest test = new JUnitTest();
		test.setName("MainTest");
		test.setFork(true);
		junit.addTest(test);

		cover.addTask(junit);

		target.addTask(cover);
		project.addOrReplaceTarget(target);

		return jacoco_exec_file.getAbsolutePath();
	}

	@Override
	public void initializeBuild(File targetPath) {
		this.execFilePath = addTestTarget(targetPath);
		// class path
		// source jar path
	}

	@Override
	public boolean runSingleTest(UnitTestScope scope) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public void initializeBuild(File targetPath, String subPomPath)
			throws IOException, BuildScriptInvalidException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void restoreBuildFile() throws IOException {
		// TODO Auto-generated method stub
		
	}
}
