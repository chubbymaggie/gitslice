package cslicer.distiller;

import ch.uzh.ifi.seal.changedistiller.model.entities.SourceCodeChange;

/**
 * A wrapper of the ChangeDistiller {@link SourceCodeChange}
 * 
 * @author Yi Li
 *
 */
public class GitRefSourceCodeChange {

	private SourceCodeChange fChange;
	private String fChangedFilePath;

	public GitRefSourceCodeChange(SourceCodeChange change,
			String changedFilePath) {
		fChange = change;
		fChangedFilePath = changedFilePath;
	}

	/**
	 * Return file path to the changed code entity.
	 * 
	 * @return changed file path
	 */
	public String getChangedFilePath() {
		return fChangedFilePath;
	}

	/**
	 * Return the ChangeDistiller {@link SourceCodeChange}.
	 * 
	 * @return {@link SourceCodeChange} object
	 */
	public SourceCodeChange getSourceCodeChange() {
		return fChange;
	}
}
