package cslicer.distiller;

/*
 * #%L
 * CSlicer
 *    ______ _____  __ _                  
 *   / ____// ___/ / /(_)_____ ___   _____
 *  / /     \__ \ / // // ___// _ \ / ___/
 * / /___  ___/ // // // /__ /  __// /
 * \____/ /____//_//_/ \___/ \___//_/
 * %%
 * Copyright (C) 2014 - 2015 Department of Computer Science, University of Toronto
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import java.io.File;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import ch.uzh.ifi.seal.changedistiller.model.entities.Delete;
import ch.uzh.ifi.seal.changedistiller.model.entities.Insert;
import ch.uzh.ifi.seal.changedistiller.model.entities.Move;
import ch.uzh.ifi.seal.changedistiller.model.entities.SourceCodeChange;
import ch.uzh.ifi.seal.changedistiller.model.entities.Update;

import com.google.inject.Guice;
import com.google.inject.Injector;

import cslicer.analyzer.ProjectConfiguration;
import cslicer.utils.PrintUtils;
import cslicer.utils.PrintUtils.TAG;

/**
 * A wrapper class of {@link GitRefFileDistiller}.
 * 
 * @author Yi Li
 */
public class GitRefDistiller {
	private List<SourceCodeChange> changes;

	public GitRefDistiller(File left, File right, String jdkVersion)
			throws ChangeDistillerException {

		try {
			if (left != null || right != null) {
				Injector injector = Guice
						.createInjector(new GitRefChangeDistillerModule());
				GitRefFileDistiller distiller = injector
						.getInstance(GitRefFileDistiller.class);

				distiller.extractClassifiedSourceCodeChanges(left, jdkVersion,
						right, jdkVersion);
				changes = distiller.getSourceCodeChanges();
			} else {
				throw new ChangeDistillerException(
						"Both versions of the file are set null!");
			}
		} catch (Exception e) {
			/*
			 * An exception most likely indicates a bug in ChangeDistiller.
			 * Please file a bug report at
			 * https://bitbucket.org/sealuzh/tools-changedistiller/issues and
			 * attach the full stack trace along with the two files that you
			 * tried to distill.
			 */
			throw new ChangeDistillerException(
					"Error occured while change distilling.", e);
		}
	}

	public GitRefDistiller(File root1, File root2)
			throws ChangeDistillerException {
		this(root1, root2, ProjectConfiguration.DEFAULT_JDK);
	}

	/**
	 * Print atomic {@link SourceCodeChange}.
	 */
	public void printChanges() {

		if (changes != null) {
			for (SourceCodeChange change : changes) {
				if (change instanceof Insert) {
					Insert ins = (Insert) change;
					PrintUtils.print("Insert: " + ins.getChangedEntity()
							+ " : " + ins.getRootEntity().getUniqueName()
							+ " : ("
							+ ins.getChangedEntity().getStartPosition() + ", "
							+ ins.getChangedEntity().getEndPosition() + ")",
							TAG.DEBUG);
				} else if (change instanceof Delete) {
					Delete del = (Delete) change;
					PrintUtils.print("Delete: " + del.getChangedEntity()
							+ " : " + del.getRootEntity().getUniqueName()
							+ " : ("
							+ del.getChangedEntity().getStartPosition() + ", "
							+ del.getChangedEntity().getEndPosition() + ")",
							TAG.DEBUG);
				} else if (change instanceof Update) {
					Update upd = (Update) change;
					PrintUtils.print("Update: " + upd.getChangedEntity()
							+ " : " + upd.getRootEntity().getUniqueName()
							+ " : ("
							+ upd.getChangedEntity().getStartPosition() + ", "
							+ upd.getChangedEntity().getEndPosition()
							+ ") --> (" + upd.getNewEntity().getStartPosition()
							+ ", " + upd.getNewEntity().getEndPosition() + ")",
							TAG.DEBUG);
				} else if (change instanceof Move) {
					Move mov = (Move) change;
					PrintUtils.print("Move: " + mov.getChangedEntity() + " : "
							+ mov.getRootEntity().getUniqueName() + " : ("
							+ mov.getChangedEntity().getStartPosition() + ", "
							+ mov.getChangedEntity().getEndPosition()
							+ ") --> (" + mov.getNewEntity().getStartPosition()
							+ ", " + mov.getNewEntity().getEndPosition() + ")",
							TAG.DEBUG);
				} else {
					PrintUtils.print("Change type not captured!", TAG.WARNING);
				}
			}
		} else {
			PrintUtils.print("No change detected!");
		}
	}

	/**
	 * Get a set of {@link SourceCodeChange}.
	 * 
	 * @return a set of atomic changes
	 */
	public Set<SourceCodeChange> getAtomicChanges() {
		if (changes == null)
			return new HashSet<SourceCodeChange>();

		return new HashSet<SourceCodeChange>(changes);
	}
}
