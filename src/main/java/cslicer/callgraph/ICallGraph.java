package cslicer.callgraph;

/*
 * #%L
 * CSlicer
 *    ______ _____  __ _                  
 *   / ____// ___/ / /(_)_____ ___   _____
 *  / /     \__ \ / // // ___// _ \ / ___/
 * / /___  ___/ // // // /__ /  __// /
 * \____/ /____//_//_/ \___/ \___//_/
 * %%
 * Copyright (C) 2014 - 2015 Department of Computer Science, University of Toronto
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import java.util.Set;

public interface ICallGraph {
	
	public void insertEdge (CGNode from, CGNode to, CGEdgeType type);
	
	public Set<MethodNode> getCaller (MethodNode method);
	
	public Set<MethodNode> getCallee (MethodNode method);
	
	public Set<MethodNode> getAccessor (FieldNode field);
	
	public Set<FieldNode> getAccessee (MethodNode method);
	
	public Set<ClassNode> getReferencer (ClassNode clazz);
	
	public Set<ClassNode> getReferencee (ClassNode clazz);
}
