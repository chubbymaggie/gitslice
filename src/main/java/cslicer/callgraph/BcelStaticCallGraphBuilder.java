package cslicer.callgraph;

/*
 * #%L
 * CSlicer
 *    ______ _____  __ _                  
 *   / ____// ___/ / /(_)_____ ___   _____
 *  / /     \__ \ / // // ___// _ \ / ___/
 * / /___  ___/ // // // /__ /  __// /
 * \____/ /____//_//_/ \___/ \___//_/
 * %%
 * Copyright (C) 2014 - 2015 Department of Computer Science, University of Toronto
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Queue;
import java.util.Set;

import javax.annotation.Nullable;

import org.apache.bcel.classfile.ClassParser;
import org.apache.bcel.classfile.JavaClass;
import org.apache.commons.io.FileUtils;

import cslicer.callgraph.ClassVisitor.DependencyLevel;
import cslicer.utils.PrintUtils;
import cslicer.utils.PrintUtils.TAG;

public class BcelStaticCallGraphBuilder extends StaticCallGraphBuilder {

	private StaticCallGraph fCallGraph;
	private Set<String> fRootClasses;

	public BcelStaticCallGraphBuilder(List<String> classPath,
			@Nullable Set<String> rootClasses) throws ClassPathInvalidException {

		fClassPath = new LinkedList<File>();

		for (String path : classPath) {
			File dir = FileUtils.getFile(path);
			if (!dir.exists() || !dir.isDirectory())
				throw new ClassPathInvalidException(path);

			fClassPath.add(dir);
		}

		fRootClasses = rootClasses;
	}

	public void buildCallGraph() {
		buildCallGraph(fRootClasses);
	}

	public BcelStaticCallGraphBuilder(String classPath,
			@Nullable Set<String> rootClasses) throws ClassPathInvalidException {
		this(Arrays.asList(classPath), rootClasses);
	}

	public BcelStaticCallGraphBuilder() {
		fClassPath = null;
		fCallGraph = new StaticCallGraph();
	}

	private void buildCallGraph(Set<String> entryClassNames) {
		fCallGraph = new StaticCallGraph();
		int counter = 0;

		// build a map from class name to BCEL class instance
		Map<String, JavaClass> nameToClassMap = new HashMap<String, JavaClass>();

		try {
			for (File cpath : fClassPath) {
				for (File entry : FileUtils.listFiles(cpath, (String[]) Arrays
						.asList("class").toArray(), true)) {
					PrintUtils.print((counter++) + " Reading class file: "
							+ entry.getName());

					ClassParser cp = new ClassParser(entry.getAbsolutePath());
					JavaClass clazz = cp.parse();

					String className = clazz.getClassName().replace("$", ".");
					nameToClassMap.put(className, clazz);
				}
			}
		} catch (IOException e) {
			PrintUtils.print(
					"Error while processing classpath: " + e.getMessage(),
					TAG.WARNING);
			e.printStackTrace();
		}

		Queue<String> workList = new LinkedList<String>();
		if (entryClassNames != null)
			workList.addAll(entryClassNames);
		else
			workList.addAll(nameToClassMap.keySet());

		Set<String> done = new HashSet<String>();

		while (!workList.isEmpty()) {
			String next = workList.poll();

			if (!nameToClassMap.containsKey(next))
				continue;

			ClassVisitor visitor = new ClassVisitor(nameToClassMap.get(next),
					fCallGraph, DependencyLevel.IGNORE_FIELDS_METHODS);
			visitor.start();
			done.add(next);
			PrintUtils.print(done.size() + ": Done: " + next);

			Set<String> successor = fCallGraph
					.getTransitiveSuccessorNames(next);
			successor.removeAll(done);
			successor.removeAll(workList);
			successor.retainAll(nameToClassMap.keySet());
			workList.addAll(successor);
		}
	}

	public StaticCallGraph getCallGraph() {
		return fCallGraph;
	}

	public void saveCallGraph(String path) {
		try {
			FileOutputStream fileOut = FileUtils.openOutputStream(FileUtils
					.getFile(path));
			ObjectOutputStream out = new ObjectOutputStream(fileOut);

			out.writeObject(fCallGraph);
			out.close();
			fileOut.close();
			PrintUtils.print("Serialized call graph is saved in " + path);
		} catch (IOException e) {
			PrintUtils.print("Save call graph failed!", TAG.WARNING);
			e.printStackTrace();
		}
	}

	public void loadCallGraph(String path) {
		try {
			FileInputStream inputFileStream = new FileInputStream(path);
			ObjectInputStream objectInputStream = new ObjectInputStream(
					inputFileStream);

			fCallGraph = (StaticCallGraph) objectInputStream.readObject();
			objectInputStream.close();
			inputFileStream.close();
			PrintUtils.print("Call graph is loaded from " + path);
		} catch (ClassNotFoundException | IOException e) {
			PrintUtils.print("Read call graph failed!", TAG.WARNING);
			e.printStackTrace();
		}
	}
}
