package cslicer.callgraph;

/*
 * #%L
 * CSlicer
 *    ______ _____  __ _                  
 *   / ____// ___/ / /(_)_____ ___   _____
 *  / /     \__ \ / // // ___// _ \ / ___/
 * / /___  ___/ // // // /__ /  __// /
 * \____/ /____//_//_/ \___/ \___//_/
 * %%
 * Copyright (C) 2014 - 2015 Department of Computer Science, University of Toronto
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

import cslicer.utils.PrintUtils;
import cslicer.utils.StatsUtils;
import cslicer.utils.graph.Edge;
import cslicer.utils.graph.Graph;
import cslicer.utils.graph.SpanningTreeVisitor;
import cslicer.utils.graph.Vertex;

public class StaticCallGraph implements ICallGraph, Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private final String JAVA_LIB_EXCLUDE = "java.*";
	private final String SUN_LIB_EXCLUDE = "com.sun.*";
	private Graph<CGNode> fGraph;

	/**
	 * Implementing a static call graph.
	 */
	public StaticCallGraph() {
		fGraph = new Graph<CGNode>();
	}

	@Override
	public void insertEdge(CGNode from, CGNode to, CGEdgeType type) {
		StatsUtils.resume("insert.edge");
		// ignore excluded nodes
		if (matchExclude(from.getName()) || matchExclude(to.getName()))
			return;

		Vertex<CGNode> fromV = fGraph.findVertexByData(from);
		if (fromV == null) {
			fromV = createVertex(from);
			fGraph.addVertex(fromV);
		}

		Vertex<CGNode> toV = fGraph.findVertexByData(to);
		if (toV == null) {
			toV = createVertex(to);
			fGraph.addVertex(toV);
		}

		fGraph.addEdge(fromV, toV, type);
		StatsUtils.stop("insert.edge");
	}

	/**
	 * Find all fields/classes/methods referenced by a node.
	 * 
	 * @param node
	 *            a {@link CGNode} in the call graph
	 * @return a set of {@link CGNode} referenced by {@code node}
	 */
	public Set<CGNode> getOutgoingNodes(CGNode node) {
		Set<CGNode> res = new HashSet<CGNode>();
		Vertex<CGNode> vertex = fGraph.findVertexByData(node);
		if (vertex != null) {
			for (Edge<CGNode> edge : vertex.getOutgoingEdges()) {
				res.add(edge.getTo().getData());
			}
		}
		return res;
	}

	/**
	 * Find all fields/classes/methods referenced by a node.
	 * 
	 * @param nodeName
	 *            the name of a call graph node
	 * @return a set of {@link CGNode} referenced
	 */
	public Set<CGNode> getOutgoingNodes(String nodeName) {
		Set<CGNode> res = new HashSet<CGNode>();
		Vertex<CGNode> vertex = fGraph.findVertexByName(nodeName);
		if (vertex != null) {
			for (Edge<CGNode> edge : vertex.getOutgoingEdges()) {
				res.add(edge.getTo().getData());
			}
		}
		return res;
	}

	/**
	 * Find all fields/classes/methods referenced by a node.
	 * 
	 * @param node
	 *            a {@link CGNode} in the call graph
	 * @return a set of {@link CGNode} which reference {@code node}
	 */
	public Set<CGNode> getIncomingNodes(CGNode node) {
		Set<CGNode> res = new HashSet<CGNode>();
		Vertex<CGNode> vertex = fGraph.findVertexByData(node);
		if (vertex != null) {
			for (Edge<CGNode> edge : vertex.getIncomingEdges()) {
				res.add(edge.getTo().getData());
			}
		}
		return res;
	}

	/**
	 * Find all fields/classes/methods referenced by a node.
	 * 
	 * @param nodeName
	 *            the name of a call graph node
	 * @return a set of {@link CGNode} which reference the given node
	 */
	public Set<CGNode> getIncomingNodes(String nodeName) {
		Set<CGNode> res = new HashSet<CGNode>();
		Vertex<CGNode> vertex = fGraph.findVertexByName(nodeName);
		if (vertex != null) {
			for (Edge<CGNode> edge : vertex.getIncomingEdges()) {
				res.add(edge.getTo().getData());
			}
		}
		return res;
	}

	/**
	 * Compute the transitive closure of all successors of a node. XXX enable
	 * caching to improve performance.
	 * 
	 * @param nodeName
	 * @return a set of {@link CGNode}
	 */
	public Set<CGNode> getTransitiveSuccessors(String nodeName) {
		Set<CGNode> res = new HashSet<CGNode>();
		Vertex<CGNode> vertex = fGraph.findVertexByName(nodeName);
		if (vertex != null) {
			SpanningTreeVisitor<CGNode> visitor = new SpanningTreeVisitor<CGNode>(
					res);
			fGraph.dfsSpanningTree(vertex, visitor);
		}
		fGraph.clearEdges();
		fGraph.clearMark();

		return res;
	}

	/**
	 * Return the names of all recursive successors of a node.
	 * 
	 * @param nodeName
	 * @return a set of recursive successor names
	 */
	public Set<String> getTransitiveSuccessorNames(String nodeName) {
		Set<String> names = new HashSet<String>();
		Set<CGNode> res = getTransitiveSuccessors(nodeName);
		for (CGNode r : res)
			names.add(r.getName());
		return names;
	}

	@Override
	public Set<MethodNode> getCaller(MethodNode method) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Set<MethodNode> getCallee(MethodNode method) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Set<MethodNode> getAccessor(FieldNode field) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Set<FieldNode> getAccessee(MethodNode method) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Set<ClassNode> getReferencer(ClassNode clazz) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Set<ClassNode> getReferencee(ClassNode clazz) {
		// TODO Auto-generated method stub
		return null;
	}

	public void printCallGraph() {
		PrintUtils.print(fGraph.toString());
	}

	private Vertex<CGNode> createVertex(CGNode node) {
		return new Vertex<CGNode>(node.getName(), node);
	}

	private boolean matchExclude(String key) {
		if (key.matches(JAVA_LIB_EXCLUDE))
			return true;
		if (key.matches(SUN_LIB_EXCLUDE))
			return true;
		return false;
	}
}
