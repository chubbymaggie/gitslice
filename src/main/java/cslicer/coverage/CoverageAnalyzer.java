package cslicer.coverage;

/*
 * #%L
 * CSlicer
 *    ______ _____  __ _                  
 *   / ____// ___/ / /(_)_____ ___   _____
 *  / /     \__ \ / // // ___// _ \ / ___/
 * / /___  ___/ // // // /__ /  __// /
 * \____/ /____//_//_/ \___/ \___//_/
 * %%
 * Copyright (C) 2014 - 2015 Department of Computer Science, University of Toronto
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import java.io.File;
import java.io.IOException;

import org.apache.commons.io.FileUtils;
import org.jacoco.core.analysis.Analyzer;
import org.jacoco.core.analysis.CoverageBuilder;
import org.jacoco.core.data.ExecutionDataStore;
import org.jacoco.core.tools.ExecFileLoader;

import com.google.inject.Guice;
import com.google.inject.Injector;

import cslicer.builder.BuildScriptInvalidException;
import cslicer.builder.BuildToolInvoker;
import cslicer.builder.UnitTestScope;
import cslicer.utils.JarUtils;
import cslicer.utils.PrintUtils;

/**
 * Run Jacoco coverage Maven plugin and collect coverage data.
 * 
 * @author Yi Li
 *
 */
public class CoverageAnalyzer {

	private ExecutionDataStore fExecData;
	private File fExecFile;
	private File fExecFileExclude;
	private BuildToolInvoker fInvoker;
	private File fSourcePath;
	private File fClassPath;

	private boolean isExecFileProvided = false;
	private boolean isExecFileExcluded = false;

	/**
	 * @param invoker
	 *            used for Maven invocation
	 * @throws BuildScriptInvalidException
	 * @throws CoverageControlIOException
	 */
	public CoverageAnalyzer(final BuildToolInvoker invoker)
			throws BuildScriptInvalidException, CoverageControlIOException {

		fInvoker = invoker;

		try {
			// manipulate pom file
			File targetPath = org.eclipse.jgit.util.FileUtils.createTempDir(
					"gitref", "-target", null);

			invoker.initializeBuild(targetPath);
			fExecFile = invoker.getExecFile();
			fClassPath = invoker.getClassPath();
		} catch (IOException e) {
			throw new CoverageControlIOException(
					"Error occured when manipulating POM file for coverage analysis!",
					e);
		}
	}

	/**
	 * Constructor.
	 * 
	 * @param execFile
	 * @param sourcePath
	 * @param classPath
	 * @throws CoverageDataMissingException
	 */
	public CoverageAnalyzer(final String execFile, final String sourcePath,
			final String classPath) throws CoverageDataMissingException {
		if (!org.codehaus.plexus.util.FileUtils.fileExists(sourcePath)
				|| !org.codehaus.plexus.util.FileUtils.fileExists(execFile)
				|| !org.codehaus.plexus.util.FileUtils.fileExists(classPath))
			throw new CoverageDataMissingException(
					"Coverage data is not found in the given path!");

		fExecFile = FileUtils.getFile(execFile);
		fInvoker = null;
		fSourcePath = FileUtils.getFile(sourcePath);
		fExecData = null;
		fClassPath = FileUtils.getFile(classPath);
		isExecFileProvided = true;
	}

	/**
	 * Constructor with two exec data dump files provided.
	 * 
	 * @param execFile1
	 * @param execFile2
	 * @param sourcePath
	 * @param classPath
	 * @throws CoverageDataMissingException
	 */
	public CoverageAnalyzer(final String execFile1, final String execFile2,
			final String sourcePath, final String classPath)
			throws CoverageDataMissingException {
		if (!org.codehaus.plexus.util.FileUtils.fileExists(sourcePath)
				|| !org.codehaus.plexus.util.FileUtils.fileExists(execFile1)
				|| !org.codehaus.plexus.util.FileUtils.fileExists(execFile2)
				|| !org.codehaus.plexus.util.FileUtils.fileExists(classPath))
			throw new CoverageDataMissingException(
					"Coverage data is not found in the given path!");

		fExecFile = FileUtils.getFile(execFile1);
		fExecFileExclude = FileUtils.getFile(execFile2);
		fInvoker = null;
		fSourcePath = FileUtils.getFile(sourcePath);
		fExecData = null;
		fClassPath = FileUtils.getFile(classPath);
		isExecFileProvided = true;
		isExecFileExcluded = true;
	}

	/**
	 * @param invoker
	 *            used for Maven invocation
	 * @param subPomFilePath
	 *            path to the sub-module POM file
	 * @throws BuildScriptInvalidException
	 * @throws CoverageControlIOException
	 */
	public CoverageAnalyzer(final BuildToolInvoker invoker,
			final String subPomFilePath) throws BuildScriptInvalidException,
			CoverageControlIOException {
		this.fInvoker = invoker;
		try {
			File targetPath = org.eclipse.jgit.util.FileUtils.createTempDir(
					"gitref", "-target", null);
			invoker.initializeBuild(targetPath, subPomFilePath);
			fExecFile = invoker.getExecFile();
			fClassPath = invoker.getClassPath();
		} catch (IOException e) {
			throw new CoverageControlIOException(
					"Error occured when manipulating POM file for coverage analysis!",
					e);
		}
	}

	/**
	 * Analyze code coverage for the test suite.
	 * 
	 * @return a {@link CoverageDatabase} with coverage data
	 * @throws CoverageControlIOException
	 */
	public final CoverageDatabase analyseCoverage()
			throws CoverageControlIOException {
		if (isExecFileProvided) {
			return processCoverageResults();
		} else {
			// run unit tests
			fInvoker.runUnitTests();
			try {
				fInvoker.restoreBuildFile();
			} catch (IOException e) {
				throw new CoverageControlIOException(
						"Error occour when restoring original POM file.", e);
			}
			return processCoverageResultsFromSourceJar(fInvoker
					.getSourceJarPath());
		}
	}

	/**
	 * Analyze code coverage for a given test method.
	 * 
	 * @param scope
	 *            {@link UnitTestScope} specifying the tests to run for coverage
	 *            analysis
	 * @return a {@link CoverageDatabase} with coverage data
	 * @throws CoverageControlIOException
	 */
	public final CoverageDatabase analyseCoverage(final UnitTestScope scope)
			throws CoverageControlIOException {
		if (scope.includeAllTest())
			return analyseCoverage();

		if (isExecFileProvided) {
			return processCoverageResults();
		}

		fInvoker.runSingleTest(scope);

		try {
			fInvoker.restoreBuildFile();
		} catch (IOException e) {
			throw new CoverageControlIOException(
					"Error occour when restoring original POM file.", e);
		}

		return processCoverageResultsFromSourceJar(fInvoker.getSourceJarPath());
	}

	public String getClassPath() {
		return fClassPath.getAbsolutePath();
	}

	/**
	 * Process coverage results. Map line number to AST nodes.
	 * 
	 * @return {@link CoverageDatabase}
	 * @throws CoverageControlIOException
	 */
	private CoverageDatabase processCoverageResultsFromSourceJar(String jarPath)
			throws CoverageControlIOException {
		extractSourceFromJar(jarPath);
		return processCoverageResults();
	}

	private CoverageDatabase processCoverageResults()
			throws CoverageControlIOException {
		ExecutionDataStore execData1 = loadExecutionData(fExecFile);
//		ExecutionDataStore execData2 = loadExecutionData(fExecFileExclude);
		
		CoverageDatabase res = computeCoverage(execData1);
//		res.exclude(computeCoverage(execData2));
		return res;
	}

	private CoverageDatabase computeCoverage(ExecutionDataStore execData)
			throws CoverageControlIOException {
		// analyze coverage data
		final CoverageBuilder builder = new CoverageBuilder();
		CoverageDatabase res = null;

		try {
			final Analyzer analyzer = new Analyzer(execData, builder);
			int numberOfClasses = analyzer.analyzeAll(fClassPath);
			PrintUtils.print(numberOfClasses + " classes have been analyzed.");

			Injector injector = Guice
					.createInjector(new CoverageDatabaseModule());
			res = injector.getInstance(CoverageDatabase.class);
			res.buildCoverageData(builder.getBundle("GITREFCOVERAGE"),
					fSourcePath);

		} catch (IOException e) {
			throw new CoverageControlIOException(
					"Error occured when analyzing "
							+ fClassPath.getAbsolutePath(), e);
		} finally {
			if (!isExecFileProvided)
				// clean up if sources are auto-gen
				FileUtils.deleteQuietly(fSourcePath);
		}

		return res;
	}

	// private CoverageDatabase computeCoverage2()
	// throws CoverageControlIOException {
	// CoverageDatabase res = null;
	// final CoverageBuilder builder = new CoverageBuilder();
	//
	// try {
	// final Analyzer analyzer = new Analyzer(fExecData, builder);
	// int numberOfClasses = analyzer.analyzeAll(fClassPath);
	// PrintUtils.print(numberOfClasses + " classes have been analyzed.");
	//
	// Injector injector = Guice
	// .createInjector(new CoverageDatabaseModule());
	// res = injector.getInstance(CoverageDatabase.class);
	// res.buildCoverageData(builder.getSourceFiles());
	// } catch (IOException e) {
	// throw new CoverageControlIOException(
	// "Error occured when analyzing "
	// + fClassPath.getAbsolutePath(), e);
	// } finally {
	// if (!isExecFileProvided)
	// // clean up if sources are auto-gen
	// FileUtils.deleteQuietly(fSourcePath);
	// }
	//
	// return res;
	// }

	private void extractSourceFromJar(String jarPath)
			throws CoverageControlIOException {
		// extract source jar file to a temporary folder
		try {
			fSourcePath = org.eclipse.jgit.util.FileUtils.createTempDir(
					"gitref", "-source-extracted", null);
			JarUtils.unzipJar(jarPath, fSourcePath);
		} catch (IOException e2) {
			throw new CoverageControlIOException(
					"Source jar file in not found in "
							+ fInvoker.getSourceJarPath(), e2);
		}
	}

	private ExecutionDataStore loadExecutionData(File execFile) throws CoverageControlIOException {
		// load execution results
		ExecutionDataStore execData = null;
		try {
			final ExecFileLoader loader = new ExecFileLoader();
			loader.load(execFile);
			execData = loader.getExecutionDataStore();
		} catch (IOException e1) {
			throw new CoverageControlIOException(
					"Jacoco .exec file is not found!", e1);
		}
		
		return execData;
	}
}
