package cslicer.jgit;

/*
 * #%L
 * CSlicer
 *    ______ _____  __ _                  
 *   / ____// ___/ / /(_)_____ ___   _____
 *  / /     \__ \ / // // ___// _ \ / ___/
 * / /___  ___/ // // // /__ /  __// /
 * \____/ /____//_//_/ \___/ \___//_/
 * %%
 * Copyright (C) 2014 - 2015 Department of Computer Science, University of Toronto
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import java.io.File;
import java.io.IOException;

import javax.annotation.Nullable;

import org.eclipse.jgit.errors.IncorrectObjectTypeException;
import org.eclipse.jgit.errors.MissingObjectException;
import org.eclipse.jgit.lib.ObjectReader;
import org.eclipse.jgit.lib.Repository;
import org.eclipse.jgit.revwalk.RevCommit;
import org.eclipse.jgit.revwalk.RevTree;
import org.eclipse.jgit.revwalk.RevWalk;
import org.eclipse.jgit.revwalk.filter.RevFilter;
import org.eclipse.jgit.treewalk.AbstractTreeIterator;
import org.eclipse.jgit.treewalk.CanonicalTreeParser;

import cslicer.utils.PrintUtils;

public class JGitUtils {

	/**
	 * @param repo
	 * @param commit
	 * @return an {@link AbstractTreeIterator}
	 * @throws IOException
	 * @throws MissingObjectException
	 * @throws IncorrectObjectTypeException
	 */
	public static AbstractTreeIterator prepareTreeParser(Repository repo,
			RevCommit commit) throws IOException, MissingObjectException,
			IncorrectObjectTypeException {
		RevWalk walk = new RevWalk(repo);
		RevTree tree = walk.parseTree(commit.getTree().getId());

		CanonicalTreeParser oldTreeParser = new CanonicalTreeParser();
		ObjectReader oldReader = repo.newObjectReader();

		try {
			oldTreeParser.reset(oldReader, tree.getId());
		} finally {
			oldReader.release();
		}

		walk.dispose();
		return oldTreeParser;
	}

	/**
	 * Determine if a repository has any commits. This is determined by checking
	 * the for loose and packed objects.
	 * 
	 * @param repo
	 * @return {@code true} if the repository has commits
	 */
	public static boolean hasCommits(Repository repo) {
		if (repo != null && repo.getDirectory().exists()) {
			return (new File(repo.getDirectory(), "objects").list().length > 2)
					|| (new File(repo.getDirectory(), "objects/pack").list().length > 0);
		}
		return false;
	}

	/**
	 * Return the merge base of two commits.
	 * 
	 * @param repo
	 * @param c1
	 * @param c2
	 * @return the merge base commit of c1 and c2
	 */
	public static @Nullable RevCommit getMergeBase(Repository repo,
			RevCommit c1, RevCommit c2) {
		RevCommit mergeBase = null;
		RevWalk walk = new RevWalk(repo);
		walk.setRevFilter(RevFilter.MERGE_BASE);

		try {
			// need to parse commits using this walker
			RevCommit cc1 = walk.parseCommit(c1.getId());
			RevCommit cc2 = walk.parseCommit(c2.getId());
			walk.markStart(cc1);
			walk.markStart(cc2);
			mergeBase = walk.next();
		} catch (IOException e) {
			PrintUtils.print("Error finding merge-base!");
			e.printStackTrace();
		} finally {
			walk.dispose();
		}

		return mergeBase;
	}

	/**
	 * A string summarizes the commit info.
	 * 
	 * @param commit
	 * @return a summary string
	 */
	public static String summary(RevCommit commit) {
		if (commit == null)
			return "";
		return commit.abbreviate(8).name() + " : " + commit.getShortMessage();
	}
}
