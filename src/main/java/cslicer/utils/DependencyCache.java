package cslicer.utils;

/*
 * #%L
 * CSlicer
 *    ______ _____  __ _                  
 *   / ____// ___/ / /(_)_____ ___   _____
 *  / /     \__ \ / // // ___// _ \ / ___/
 * / /___  ___/ // // // /__ /  __// /
 * \____/ /____//_//_/ \___/ \___//_/
 * %%
 * Copyright (C) 2014 - 2015 Department of Computer Science, University of Toronto
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.eclipse.jgit.revwalk.RevCommit;

import cslicer.utils.graph.Graph;
import cslicer.utils.graph.SpanningTreeVisitor;
import cslicer.utils.graph.Vertex;

public class DependencyCache {

	private Graph<RevCommit> fDeps;
	private Set<String> fDone; // dependency computed

	public DependencyCache() {
		fDeps = new Graph<RevCommit>();
		fDone = new HashSet<String>();
	}

	public void addDirectDeps(RevCommit c, Set<RevCommit> deps) {
		Vertex<RevCommit> fromV = fDeps.findVertexByName(c.getName());

		if (fromV == null) {
			fromV = createVertex(c);
			fDeps.addVertex(fromV);
		}

		for (RevCommit d : deps) {
			Vertex<RevCommit> toV = fDeps.findVertexByName(d.getName());

			if (toV == null) {
				toV = createVertex(d);
				fDeps.addVertex(toV);
			}
			fDeps.addEdge(fromV, toV, DependencyEdgeType.HUNK_DEPS);
		}

		fDone.add(c.getName());
	}

	private Vertex<RevCommit> createVertex(RevCommit c) {
		return new Vertex<RevCommit>(c.getName(), c);
	}

	public boolean directDepsComputed(RevCommit c) {
		return fDone.contains(c.getName());
	}

	public Set<RevCommit> getTransitiveDeps(RevCommit c) {
		Set<RevCommit> res = new HashSet<RevCommit>();
		Vertex<RevCommit> vertex = fDeps.findVertexByName(c.getName());
		if (vertex != null) {
			SpanningTreeVisitor<RevCommit> visitor = new SpanningTreeVisitor<RevCommit>(
					res);
			fDeps.dfsSpanningTree(vertex, visitor);
		}
		fDeps.clearEdges();
		fDeps.clearMark();

		return res;
	}

	public List<RevCommit> getSortedDeps(boolean reverse) {
		return fDeps.topologicalSort(reverse);
	}

	public Set<RevCommit> getUnSortedDeps() {
		Set<RevCommit> res = new HashSet<RevCommit>();

		for (Vertex<RevCommit> v : fDeps.getVerticies())
			res.add(v.getData());
		return res;
	}

	@Override
	public String toString() {
		return "Dependency Graph:\n" + fDeps.toString();
	}
}
