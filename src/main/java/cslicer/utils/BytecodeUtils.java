package cslicer.utils;

/*
 * #%L
 * CSlicer
 *    ______ _____  __ _                  
 *   / ____// ___/ / /(_)_____ ___   _____
 *  / /     \__ \ / // // ___// _ \ / ___/
 * / /___  ___/ // // // /__ /  __// /
 * \____/ /____//_//_/ \___/ \___//_/
 * %%
 * Copyright (C) 2014 - 2015 Department of Computer Science, University of Toronto
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import org.apache.commons.lang3.StringUtils;
import org.objectweb.asm.Type;

public class BytecodeUtils {

	private final static String GENERIC_FILTER = "<[\\p{L}][\\p{L}\\p{N}]*>";

	public static String getQualifiedClassName(final String vmname) {
		Type object = Type.getObjectType(vmname);
		return object.getClassName().replace('/', '.').replace('$', '.');
	}

	public static String getQualifiedFieldName(String fieldName,
			String className, String fieldType) {
		return getQualifiedClassName(className) + "." + fieldName + " : "
				+ getShortClassName(fieldType);
	}

	public static String getQualifiedMethodName(final String vmclassname,
			final String vmmethodname, final String vmdesc) {
		return getQualifiedClassName(vmclassname) + "."
				+ getMethodName(vmclassname, vmmethodname, vmdesc, false);
	}

	public static String filterGenericType(String astName) {
		return astName.replaceAll(GENERIC_FILTER, StringUtils.EMPTY);
	}

	private static String getMethodName(final String vmclassname,
			final String vmmethodname, final String vmdesc,
			boolean qualifiedParams) {

		if ("<clinit>".equals(vmmethodname)) {
			return "static {...}";
		}
		final StringBuilder result = new StringBuilder();
		if ("<init>".equals(vmmethodname)) {
			if (isAnonymous(vmclassname)) {
				return "{...}";
			} else {
				result.append(getShortClassName(vmclassname));
			}
		} else {
			result.append(vmmethodname);
		}
		result.append('(');
		final Type[] arguments = Type.getArgumentTypes(vmdesc);
		boolean comma = false;
		for (final Type arg : arguments) {
			if (isInnerClass(vmclassname)
					&& arg.getClassName().equals(
							getOutterClassName(vmclassname)))
				continue;

			if (comma) {
				result.append(",");
			} else {
				comma = true;
			}
			if (qualifiedParams) {
				result.append(getClassName(arg.getClassName()));
			} else {
				result.append(getShortTypeName(arg));
			}
		}
		result.append(')');
		return result.toString();
	}

	public static String getShortClassName(final String vmname) {
		final String name = getClassName(vmname);
		return name.substring(name.lastIndexOf('.') + 1);
	}

	private static String getClassName(final String vmname) {
		final int pos = vmname.lastIndexOf('/');
		final String name = pos == -1 ? vmname : vmname.substring(pos + 1);
		return name.replace('$', '.');
	}

	private static boolean isAnonymous(final String vmname) {
		final int dollarPosition = vmname.lastIndexOf('$');
		if (dollarPosition == -1) {
			return false;
		}
		final int internalPosition = dollarPosition + 1;
		if (internalPosition == vmname.length()) {
			// shouldn't happen for classes compiled from Java source
			return false;
		}
		// assume non-identifier start character for anonymous classes
		final char start = vmname.charAt(internalPosition);
		return !Character.isJavaIdentifierStart(start);
	}

	private static Object getOutterClassName(String vmclassname) {
		final int pos = vmclassname.lastIndexOf('$');
		final String res = pos == -1 ? getClassName(vmclassname)
				: getClassName(vmclassname.substring(0, pos));
		return res;
	}

	private static boolean isInnerClass(String vmclassname) {
		return vmclassname.lastIndexOf('$') != -1;
	}

	private static String getShortTypeName(final Type type) {
		final String name = type.getClassName();
		final int pos = name.lastIndexOf('.');
		final String shortName = pos == -1 ? name : name.substring(pos + 1);
		return shortName.replace('$', '.');
	}
}
