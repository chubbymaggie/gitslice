package cslicer.jgit;

/*
 * #%L
 * CSlicer
 *    ______ _____  __ _                  
 *   / ____// ___/ / /(_)_____ ___   _____
 *  / /     \__ \ / // // ___// _ \ / ___/
 * / /___  ___/ // // // /__ /  __// /
 * \____/ /____//_//_/ \___/ \___//_/
 * %%
 * Copyright (C) 2014 - 2015 Department of Computer Science, University of Toronto
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import static org.junit.Assert.assertTrue;

import java.util.Collections;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.lang3.tuple.Pair;
import org.eclipse.jgit.revwalk.RevCommit;
import org.junit.After;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

import cslicer.TestUtils;
import cslicer.utils.PrintUtils;

public class CherryPickingTest {
	JGit repo;
	Map<String, Pair<String, String>> feature;
	Set<RevCommit> pool;

	@Before
	public void setUp() throws Exception {
		repo = new JGit("/home/liyi/bit/hadoop/.git");
		feature = TestUtils.readHadoop6581();
		pool = new HashSet<RevCommit>();
		for (String c : feature.keySet())
			pool.add(repo.getCommit(c));
	}

	@After
	public void tearDown() throws Exception {

	}

	@Ignore
	@Test
	public void pickFeatureAll() throws Exception {
		List<RevCommit> commits = getFeatureCommits(false);

		for (RevCommit c : commits) {
			PrintUtils.print(JGitUtils.summary(c));
		}
		PrintUtils.print("Linear history has " + commits.size() + " commits.");

		repo.checkOutNewBranch("feature", commits.get(0));
		assertTrue(repo.pickCommitsToBranch("feature", commits, true,
				Collections.<String> emptySet()));
	}

	@Ignore
	@Test
	public void pickFeatureRes() throws Exception {
		List<RevCommit> commits = getFeatureCommits(true);

		List<RevCommit> hunks = TestUtils
				.computeHunkDepSet(repo, commits, pool);
		Set<String> keys = new HashSet<String>();

		for (RevCommit h : hunks) {
			keys.add(h.name());
			PrintUtils.print(JGitUtils.summary(h));
		}

		for (String f : feature.keySet()) {
			if (!keys.contains(f))
				PrintUtils.print(f);
		}

		repo.checkOutNewBranch("feature-res", hunks.get(0));
		assertTrue(repo.pickCommitsToBranch("feature-res", hunks, true,
				Collections.<String> emptySet()));
	}

	private List<RevCommit> getFeatureCommits(boolean skipFF)
			throws CommitNotFoundException {
		List<RevCommit> commits = new LinkedList<RevCommit>();
		for (String c : feature.keySet()) {

			if (feature.get(c).getLeft().equals("F")
					|| feature.get(c).getLeft().equals("R")) {
				// PrintUtils.print(c);
				commits.add(repo.getCommit(c));
			}

			if (!skipFF && feature.get(c).getLeft().equals("M")) {
				RevCommit resolve = repo.getCommit(c);
				RevCommit base = JGitUtils.getMergeBase(repo.getRepo(),
						resolve.getParent(1), resolve.getParent(0));
				commits.addAll(repo.getCommitList(base, resolve.getParent(1),
						false));
			}
		}

		Collections.reverse(commits);
		return commits;
	}
}
