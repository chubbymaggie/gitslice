![cslicer.png](https://bitbucket.org/repo/Een7Xx/images/3832351779-cslicer.png)

This is the source directory of CSlicer, an automatic semantic slicing tool for Java projects hosted in Git repositories.

## License

The license is in LICENSE.txt

## Introduction

CSlicer is a semantic slicing tool for Git reposiotries under active development by the Software
Engineering group at the University of Toronto.
The goal of semantic slicing is to identify a subset of change sets (commits) that implementing a software functionality (e.g., feature, enhancement, or bug fix). The applications include functionality migration, branch refactoring, etc.
CSlicer currently works only for Java repositories.

## Dependencies

### Maven ###

[Apache Maven](http://maven.apache.org) is used for dependency control. To build the project, first download and install Maven following the instructions [here](http://maven.apache.org/download.cgi) (version >= 3.2). Make sure environment variables ```M2_HOME```, ```M2```, and ```JAVA_HOME``` are set correctly according to the instructions.
All required dependencies will be fetched and installed by Maven automatically.

### ChangeDistiller ###

We use the open source project [ChangeDistiller](https://bitbucket.org/sealuzh/tools-changedistiller/wiki/Home) for categorizing and analyzing changes.
A local copy of ChangeDistiller-SNAPSHOT-0.0.1 is included in the source directory.

## Install

Requirements:

* JDK 1.7+
* Maven 3.0 or later
* Internet connection for first build (to fetch all Maven and CSlicer dependencies)

Get the source:

```
git clone https://liyistc@bitbucket.org/liyistc/gitslice.git cslicer
```

Maven build goals:

* Clean: ```mvn clean```
* Compile: ```mvn compile```
* Run tests: ```mvn test```
* Create JAR: ```mvn package```
* Install JAR in M2 cache: ```mvn install```
* Build javadocs: ```mvn javadoc:javadoc```

Build options:

* Use ```-DskipTests``` to skip tests

## Run CSlicer ##

Find executable jar files at ```cslicer/target``` and all dependencies at ```cslicer/target/lib```.

Run CSlicer using ```java -jar cslicer-XXX-jar-with-dependencies.jar -r <PATH_TO_REPO> -c <TARGET_COMMIT> -b <MAVEN_BUILD_SCRIPT>```.

See detailed usage by entering the command ```java -jar cslicer-XXX-jar-with-dependencies.jar -help```

## Under Construction ..